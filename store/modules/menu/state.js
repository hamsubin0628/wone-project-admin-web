const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            parentIcon: 'el-icon-tickets',
            menuLabel: [
                { id: 'DASH_BOARD',  currentName: 'Dash Board', link: '/', isShow: true },
                { id: 'ADMIN_INFO',  currentName: '관리자 정보', link: '/admin-info', isShow: true },
                { id: 'SERVICE_SETTING',  currentName: '서비스 관리', link: '/service-setting', isShow: true },
                { id: 'USER_LIST',  currentName: '서비스 관리', link: '/user-list', isShow: true },
                { id: 'BOARD',  currentName: '게시판', link: '/board-ask', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            parentIcon: 'el-icon-tickets',
            menuLabel: [
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '회원등록', link: '/', isShow: false },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '회원수정', link: '/', isShow: false },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '회원리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: false },
            ]
        },
        {
            parentName: '마이 메뉴',
            parentIcon: 'el-icon-tickets',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
